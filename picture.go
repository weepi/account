package main

import (
	"bytes"
	"crypto/md5"
	"errors"
	"fmt"
	pb "gitlab.com/weepi/account/proto"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	MaxPictureSize = 1024 * 1024
)

var (
	ErrS3PictureUpload = errors.New("uploading picture error")
)

func (s *WeepiAccount) UploadProfilePicture(ctx context.Context, req *pb.Picture) (*pb.PictureUrl, error) {
	if len(req.GetPicture())/8 > MaxPictureSize {
		Logger.Warn("picture size too large")
		return nil, grpc.Errorf(codes.InvalidArgument, "picture size too large. max %d bytes", MaxPictureSize)
	}

	// TODO: Check if this is a JPEG image

	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	// Upload to s3
	uploadOutput, err := s.s3Uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String("weepen-user.profile.picture"),
		ACL:    aws.String("public-read"),
		Key:    aws.String(fmt.Sprintf("%s_%x", userUUID.String(), md5.Sum(req.GetPicture()))),
		Body:   bytes.NewReader(req.GetPicture()),
	})
	if err != nil {
		Logger.With("error", err).Error(ErrS3PictureUpload.Error())
		return nil, grpc.Errorf(codes.Internal, ErrS3PictureUpload.Error())
	}

	// Set returned URL in user account
	if _, err := s.UpdatePictureURL(ctx, &pb.PictureUrl{
		PictureUrl: uploadOutput.Location,
	}); err != nil {
		return nil, err
	}

	return &pb.PictureUrl{
		PictureUrl: uploadOutput.Location,
	}, nil
}
