RM		= rm -f
BINARY		= account
PROTO_FOLDER	= proto
IMAGE_NAME	= weepi/${BINARY}
PKG 		:= "gitlab.com/weepi/$(BINARY)"

$(BINARY):
	go build -v $(PKG)

install:
	go install -v

proto:
	protoc -I/usr/local/include \
		-I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--go_out=plugins=grpc:. \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		$(PROTO_FOLDER)/account.proto
	cd $(PROTO_FOLDER); go get .

dep:
	rm -rf Godeps vendor && godep save

image:
	docker build -t $(IMAGE_NAME) .

all: $(BINARY)

clean:
	$(RM) $(BINARY)

re: clean all

.PHONY: $(BINARY) all re $(PROTO_FOLDER)
