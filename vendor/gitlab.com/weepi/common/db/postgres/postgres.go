package postgres

import (
	"github.com/lib/pq"
)

const (
	UniqueViolationCode     = "23505"
	ForeignKeyViolationCode = "23503"
)

func IsUniqueConstraintError(err error, constraintName string) bool {
	if pqErr, ok := err.(*pq.Error); ok {
		return pqErr.Code == UniqueViolationCode && pqErr.Constraint == constraintName
	}
	return false
}

func IsForeignKeyConstraintError(err error, constraintName string) bool {
	if pqErr, ok := err.(*pq.Error); ok {
		return pqErr.Code == ForeignKeyViolationCode && pqErr.Constraint == constraintName
	}
	return false

}
