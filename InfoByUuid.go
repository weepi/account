package main

import (
	pb "gitlab.com/weepi/account/proto"
	"gitlab.com/weepi/account/schemes"

	pbtimestamp "github.com/golang/protobuf/ptypes/timestamp"
	"golang.org/x/net/context"
)

func (s *WeepiAccount) InfoByUuid(ctx context.Context, req *pb.Uuid) (*pb.Account, error) {
	var user schemes.User

	if s.db.Where("uuid = ?", req.GetUuid()).First(&user).RecordNotFound() {
		Logger.Errorf("user '%s' not in DB", req.GetUuid())
		return nil, ErrUserUuidNotFound
	}

	if s.db.Error != nil {
		Logger.With("error", s.db.Error).Errorf("uuid '%s' access error", req.GetUuid())
		return nil, ErrUserUuidNotFound
	}

	var sports []*pb.Uuid
	for _, spn := range user.Sports {
		sports = append(sports, &pb.Uuid{Uuid: spn})
	}

	return &pb.Account{
		FirstName: &pb.FirstName{
			FirstName: user.FirstName,
		},
		MobilePhoneNumber: &pb.MobilePhoneNumber{
			MobilePhoneNumber: user.MobilePhoneNumber,
		},
		DateOfBirth: &pb.DateOfBirth{
			DateOfBirth: &pbtimestamp.Timestamp{
				Seconds: user.DateOfBirth.Unix(),
			},
		},
		EmailAddress: &pb.EmailAddress{
			EmailAddress: user.Email,
		},
		Location: &pb.GPSCoordinates{
			Latitude:  user.Latitude,
			Longitude: user.Longitude,
		},
		PictureUrl: &pb.PictureUrl{
			PictureUrl: user.PictureURL,
		},
		SportsUUIDs: sports,
	}, nil
}
