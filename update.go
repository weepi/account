package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	pb "gitlab.com/weepi/account/proto"
	"gitlab.com/weepi/account/schemes"
	pbauth "gitlab.com/weepi/auth/proto"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/goware/emailx"
	"github.com/satori/go.uuid"
	"gitlab.com/weepi/common/db/postgres"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	ErrCouldNotUpdate                 = errors.New("could not update the attribute")
	ErrUserUuidNotFound               = errors.New("user UUID not found")
	ErrInvalidUuid                    = errors.New("invalid user UUID")
	ErrMobilePhoneNumberDoesNotExists = errors.New("mobile phone does not exists")
	ErrMobilePhoneNumberAlreadyTaken  = errors.New("mobile phone number is already taken")
	ErrEmailAddressDoesNotExists      = errors.New("email address does not exists")
	ErrEmailAddressAlreadyTaken       = errors.New("email address is already taken")
)

// Get UUID from context
func getValidUserUUID(ctx context.Context) (uuid.UUID, error) {
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return uuid.Nil, status.Errorf(codes.Unauthenticated, "missing context metadata")
	}
	if len(meta["useruuid"]) != 1 {
		//s.logger.Debug("could not find userUuid in meta")
		return uuid.Nil, ErrUserUuidNotFound
	}
	userUUID, err := uuid.FromString(meta["useruuid"][0])
	if err != nil {
		return uuid.Nil, ErrInvalidUuid
	}
	return userUUID, nil
}

func isDbError(s WeepiAccount) error {
	if s.db.Error != nil {
		Logger.With("error", s.db.Error).Error("could not update")
		return ErrCouldNotUpdate
	}
	return nil
}

func (s *WeepiAccount) UpdateFirstName(ctx context.Context, req *pb.FirstName) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	if err := validation.Validate(
		req.GetFirstName(),
		validation.Required,
		validation.Length(FirstNameMinLen, FirstNameMaxLen),
		is.UTFLetter); err != nil {
		return nil, ErrInvalidName
	}

	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("first_name", req.GetFirstName()).RecordNotFound() {
		Logger.Debugf("user with uuid '%s' not found", userUUID.String())
		return nil, ErrCouldNotUpdate
	}

	if err := isDbError(*s); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAccount) UpdateMobilePhoneNumber(ctx context.Context, req *pb.MobilePhoneNumber) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	mpn := req.GetMobilePhoneNumber()
	if mpn == "" {
		return nil, ErrInvalidPhoneNumber
	}

	/*	if err := validation.Validate(
		mpn,
		validation.Required,
		validation.Length(10, 15),
		is.Digit); err != nil {
		return nil, ErrInvalidPhoneNumber
	}*/

	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("mobile_phone_number", mpn).RecordNotFound() {
		return nil, ErrMobilePhoneNumberDoesNotExists
	}
	if postgres.IsUniqueConstraintError(s.db.Error, UniqueConstraintMobilePhoneNumber) {
		return nil, ErrMobilePhoneNumberAlreadyTaken
	}
	if err := isDbError(*s); err != nil {
		Logger.With("error", err).Error("updating mobile phone number db error")
		return nil, ErrCouldNotUpdate
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAccount) UpdateDateOfBirth(ctx context.Context, req *pb.DateOfBirth) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	dateOfBirth := time.Unix(req.GetDateOfBirth().GetSeconds(), 0)

	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("date_of_birth", dateOfBirth).RecordNotFound() {
		Logger.Debugf("user with uuid '%s' not found", userUUID.String())
		return nil, ErrCouldNotUpdate
	}
	if err := isDbError(*s); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAccount) UpdatePassword(ctx context.Context, req *pb.Secret) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	creds := &pbauth.Creds{
		Uuid: &pbauth.Uuid{
			Value: userUUID.String(),
		},
		Secret: &pbauth.Secret{
			Value: req.GetSecret(),
		},
	}
	var errn error
	if err := s.msg.Request("auth.update", creds, &errn, 3*time.Second); err != nil {
		return nil, err
	}

	return &empty.Empty{}, errn
}

func (s *WeepiAccount) UpdateEmailAddress(ctx context.Context, req *pb.EmailAddress) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	emailAddress := req.GetEmailAddress()

	if err := emailx.Validate(emailAddress); err != nil {
		return nil, err
	}

	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("email", emailAddress).RecordNotFound() {
		return nil, ErrEmailAddressDoesNotExists
	}
	if postgres.IsUniqueConstraintError(err, UniqueConstraintEmail) {
		return nil, ErrEmailAddressAlreadyTaken
	}
	if err := isDbError(*s); err != nil {
		Logger.With("error", err).Error("updating email address db error")
		return nil, ErrCouldNotUpdate
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAccount) UpdateGPSCoordinates(ctx context.Context, req *pb.GPSCoordinates) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	latitude := req.GetLatitude()
	longitude := req.GetLongitude()

	if errLon, errLat := validation.Validate(strconv.FormatFloat(longitude, 'f', -1, 64), is.Longitude),
		validation.Validate(strconv.FormatFloat(latitude, 'f', -1, 64), is.Latitude); errLon != nil || errLat != nil {
		return nil, fmt.Errorf(strings.Join([]string{errLon.Error(), errLat.Error()}, ""))
	}

	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("latitude", latitude).RecordNotFound() ||
		s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("longitude", longitude).RecordNotFound() {
		Logger.Debugf("user with uuid '%s' not found", userUUID.String())
		return nil, ErrCouldNotUpdate
	}

	if err := isDbError(*s); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAccount) UpdatePictureURL(ctx context.Context, req *pb.PictureUrl) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	pictureUrl := req.GetPictureUrl()

	if err := validation.Validate(pictureUrl, is.URL, is.RequestURL); err != nil {
		return nil, ErrInvalidPictureUrl
	}

	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("picture_url", pictureUrl).RecordNotFound() {
		Logger.Debugf("user with uuid '%s' not found", userUUID.String())
		return nil, ErrCouldNotUpdate
	}
	if err := isDbError(*s); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *WeepiAccount) UpdateSports(ctx context.Context, req *pb.Sports) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	var sports []string
	for _, spn := range req.GetSports() {
		sports = append(sports, spn.GetName())
	}
	if s.db.Model(&schemes.User{}).Where("uuid = ?", userUUID.String()).Update("sports", sports).RecordNotFound() {
		Logger.Debugf("user with uuid '%s' not found", userUUID.String())
		return nil, ErrCouldNotUpdate
	}
	if err := isDbError(*s); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}
