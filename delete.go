package main

import (
	"errors"
	"time"

	"gitlab.com/weepi/account/schemes"
	"gitlab.com/weepi/common/db"
	"gitlab.com/weepi/common/utils"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

func deleteAccount(db *db.DB, userUUID uuid.UUID) error {
	if err := db.Where("uuid = ?", userUUID).Delete(schemes.User{}).Error; err != nil {
		return errors.New("This account could not be deleted")
	}
	return nil
}

func (s *WeepiAccount) Delete(ctx context.Context, _ *empty.Empty) (*empty.Empty, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	var errn error
	if err = s.msg.Request("auth.delete", userUUID, &errn, 1*time.Second); err != nil {
		Logger.Errorf("deleting account with user UUID: %s, error: %s", userUUID.String(), err)
		return nil, grpc.Errorf(codes.Internal, "internal error")
	}

	var errnb utils.Error
	if err := s.msg.Request("event.deleteUser", userUUID.String(), &errnb, 3*time.Second); err != nil {
		Logger.Error("Error deleting user events")
		return nil, err
	}
	if errnb.Msg != "" {
		return nil, errors.New(errnb.Msg)
	}

	Logger.Infof("Deleting account with user UUID: %s", userUUID.String())
	if err = deleteAccount(s.db, userUUID); err != nil {
		Logger.Errorf("Deleting account with user UUID: %s, error: %s", userUUID.String(), err)
		return nil, err
	}

	Logger.Infof("Account with user UUID: %s deleted", userUUID.String())
	return &empty.Empty{}, errn
}
