package schemes

import (
	"time"

	"github.com/lib/pq"
	"gitlab.com/weepi/common/db/postgres/schemes"
)

type User struct {
	schemes.Model
	FirstName         string `gorm:"not null"`
	MobilePhoneNumber string `gorm:"default:null;unique"`
	DateOfBirth       time.Time
	Email             string `gorm:"default:null;unique"`
	Latitude          float64
	Longitude         float64
	PictureURL        string
	Sports            pq.StringArray `gorm:"type:text[]"`
}
