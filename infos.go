package main

import (
	"errors"

	pb "gitlab.com/weepi/account/proto"
	"gitlab.com/weepi/account/schemes"

	"github.com/golang/protobuf/ptypes/empty"
	pbtimestamp "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/jinzhu/gorm"
	"golang.org/x/net/context"
)

var (
	ErrPhoneNumberNotFound  = errors.New("phone number not found")
	ErrEmailAddressNotFound = errors.New("email address not found")
)

func isExistingAccount(request *gorm.DB) error {
	if request.RecordNotFound() {
		return errors.New("Account not found")
	}
	if request.Error != nil {
		return errors.New("An error occured")
	}
	return nil
}

func (s *WeepiAccount) Infos(ctx context.Context, _ *empty.Empty) (*pb.Account, error) {
	userUUID, err := getValidUserUUID(ctx)
	if err != nil {
		return nil, err
	}

	account := &schemes.User{}
	request := s.db.Where("uuid = ?", userUUID).First(&account)
	if err := isExistingAccount(request); err != nil {
		return nil, err
	}

	// Fill sports answer
	var sports []*pb.Uuid
	for _, spn := range account.Sports {
		sports = append(sports, &pb.Uuid{Uuid: spn})
	}

	return &pb.Account{
		FirstName: &pb.FirstName{
			FirstName: account.FirstName,
		},
		MobilePhoneNumber: &pb.MobilePhoneNumber{
			MobilePhoneNumber: account.MobilePhoneNumber,
		},
		DateOfBirth: &pb.DateOfBirth{
			DateOfBirth: &pbtimestamp.Timestamp{
				Seconds: account.DateOfBirth.Unix(),
			},
		},
		EmailAddress: &pb.EmailAddress{
			EmailAddress: account.Email,
		},
		Location: &pb.GPSCoordinates{
			Latitude:  account.Latitude,
			Longitude: account.Longitude,
		},
		PictureUrl: &pb.PictureUrl{
			PictureUrl: account.PictureURL,
		},
		SportsUUIDs: sports,
	}, nil
}

func (s *WeepiAccount) MobilePhoneNumberToUuid(ctx context.Context, req *pb.MobilePhoneNumber) (*pb.Uuid, error) {
	var user schemes.User
	if s.db.Where("mobile_phone_number = ?", req.GetMobilePhoneNumber()).First(&user).RecordNotFound() {
		Logger.Errorf("number phone '%s' not in DB", req.GetMobilePhoneNumber())
		return nil, ErrPhoneNumberNotFound
	}
	if s.db.Error != nil {
		Logger.With("error", s.db.Error).Errorf("number phone '%s' access error", req.GetMobilePhoneNumber())
		return nil, ErrPhoneNumberNotFound
	}
	return &pb.Uuid{
		Uuid: user.UUID.String(),
	}, nil
}

func (s *WeepiAccount) EmailToUuid(ctx context.Context, req *pb.EmailAddress) (*pb.Uuid, error) {
	var user schemes.User
	if s.db.Where("email = ?", req.GetEmailAddress()).First(&user).RecordNotFound() {
		Logger.Errorf("email address '%s' not in DB", req.GetEmailAddress())
		return nil, ErrEmailAddressNotFound
	}
	if s.db.Error != nil {
		Logger.With("error", s.db.Error).Errorf("email address '%s' access error", req.GetEmailAddress())
		return nil, ErrEmailAddressNotFound
	}
	return &pb.Uuid{
		Uuid: user.UUID.String(),
	}, nil
}
