# Weepi account !

The weepi account microservice was used by [Weepen](https://weepen.io/) to manage users accounts.

The docker compose file in the [deploy repository](https://gitlab.com/weepi/deploy) was used to manage it.
