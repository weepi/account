package main

import (
	"time"

	authp "gitlab.com/weepi/auth/proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

func (s *WeepiAccount) AuthInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	Logger.Debugf("Method '%s' called", info.FullMethod)

	// Non authenticated methods
	switch info.FullMethod {
	case "/WeepiAccount/Create", "/WeepiAccount/MobilePhoneNumberToUuid", "/WeepiAccount/EmailToUuid", "/WeepiAccount/InfoByUuid":
		return handler(ctx, req)
	default:
	}

	// Pass extract and pass token to weepi auth
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "missing context metadata")
	}

	if len(meta["authorization"]) == 0 {
		Logger.Debug("could not find authorization in meta")
		return nil, grpc.Errorf(codes.Unauthenticated, "invalid token")
	}

	md := metadata.Pairs(
		"authorization", meta["authorization"][0],
	)
	ctx = metadata.NewOutgoingContext(ctx, md)

	var auth authp.Auth
	// Check if authenticated by calling weepi auth
	err := s.msg.Request("auth.check", meta["authorization"][0], &auth, 1*time.Second)
	if err != nil {
		Logger.With("error", err).Error("checking auth internal error")
		return nil, grpc.Errorf(codes.Unauthenticated, "not authenticated")
	}

	if !auth.GetIsAuth() {
		Logger.Debugf("wrong auth %+v", auth)
		return nil, grpc.Errorf(codes.Unauthenticated, "not authenticated")
	}

	// Set user uuid in meta
	md = metadata.Join(md, metadata.Pairs("useruuid", auth.GetUuid().GetValue()))
	ctx = metadata.NewIncomingContext(ctx, md)

	return handler(ctx, req)
}
