package main

import (
	"flag"
	"log"
	"time"

	pb "gitlab.com/weepi/account/proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1664", "Endpoint to listen to")
	uuidFlag     = flag.String("uuid", "", "User UUID")
	tokenFlag    = flag.String("token", "", "User token")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAccountClient(conn)

	// Prepare account creation infos

	uuid := &pb.Uuid{
		Uuid: *uuidFlag,
	}

	// If the creation took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	md := metadata.Pairs("authorization", *tokenFlag)
	ctx = metadata.NewOutgoingContext(ctx, md)

	// Ask to create an account
	_, err = client.Delete(ctx, uuid)
	if err != nil {
		log.Fatalf("Error deleting account : %v", err)
	}

	// Account created!
	log.Printf("Account deleted")
}
