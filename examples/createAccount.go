package main

import (
	"flag"
	"log"
	"time"

	pbtimestamp "github.com/golang/protobuf/ptypes/timestamp"
	pb "gitlab.com/weepi/account/proto"

	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1664", "Endpoint to listen to")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAccountClient(conn)

	// Prepare account creation infos
	accountInfos := &pb.Account{
		FirstName: &pb.FirstName{
			FirstName: "The",
		},
		MobilePhoneNumber: &pb.MobilePhoneNumber{
			MobilePhoneNumber: "0000005400",
		},
		DateOfBirth: &pb.DateOfBirth{
			DateOfBirth: &pbtimestamp.Timestamp{
				Seconds: 788918400, // 01/01/1995
			},
		},
		Secret: &pb.Secret{
			Secret: "Passw0rd_",
		},
		EmailAddress: &pb.EmailAddress{
			EmailAddress: "super.blue.turtle@yopmail.com",
		},
		Location: &pb.GPSCoordinates{
			Latitude:  35.21076593772987,
			Longitude: 11.22855348629825,
		},
		PictureUrl: &pb.PictureUrl{
			PictureUrl: "https://salade.fr/laitue.png",
		},
		AuthType: pb.AccountAuthType(0),
	}
	accountInfos.Sports = append(accountInfos.Sports, &pb.Sport{Name: "running"})
	accountInfos.Sports = append(accountInfos.Sports, &pb.Sport{Name: "tennis"})

	// If the creation took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
	defer cancel()

	md := metadata.Pairs("authorization", "cGF0YXRl")
	ctx = metadata.NewOutgoingContext(ctx, md)

	// Ask to create an account
	res, err := client.Create(ctx, accountInfos)
	if err != nil {
		log.Fatalf("Error creating account : %v", err)
	}

	// Get User UUID
	userUUID, err := uuid.FromString(res.GetUuid())
	if err != nil {
		log.Fatalf("Could not convert user UUID: %s", err)
	}

	// Account created!
	log.Printf("Account created, user ID : %s", userUUID)
}
