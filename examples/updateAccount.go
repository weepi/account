package main

import (
	"flag"
	"log"
	"time"

	pbtimestamp "github.com/golang/protobuf/ptypes/timestamp"
	pb "gitlab.com/weepi/account/proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1664", "Endpoint to listen to")
	tokenFlag    = flag.String("token", "", "User token")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAccountClient(conn)

	// If the query took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	md := metadata.Pairs("authorization", *tokenFlag)
	ctx = metadata.NewOutgoingContext(ctx, md)

	if _, err = client.UpdateFirstName(ctx, &pb.FirstName{
		FirstName: "Kiki",
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	if _, err = client.UpdateMobilePhoneNumber(ctx, &pb.MobilePhoneNumber{
		MobilePhoneNumber: "33707070707",
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	if _, err = client.UpdateEmailAddress(ctx, &pb.EmailAddress{
		EmailAddress: "kiki@yopmail.com",
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	if _, err = client.UpdateDateOfBirth(ctx, &pb.DateOfBirth{
		DateOfBirth: &pbtimestamp.Timestamp{
			Seconds: 788918401, // 01/01/1995
		},
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	if _, err = client.UpdateGPSCoordinates(ctx, &pb.GPSCoordinates{
		Latitude:  15.21076593772987,
		Longitude: 11.22855348629825,
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	if _, err = client.UpdatePictureURL(ctx, &pb.PictureUrl{
		PictureUrl: "https://vignette.wikia.nocookie.net/disney/images/8/86/Kiki_07.jpg",
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	if _, err = client.UpdateSports(ctx, &pb.Sports{
		Sports: []*pb.Sport{
			&pb.Sport{Name: "squash"},
			&pb.Sport{Name: "nap"},
		},
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	/*
		// Must be called at the end
		if _, err = client.UpdatePassword(ctx, &pb.Password{
			Password: "Turtle_3",
		}); err != nil {
			log.Fatalf("Error updating account : %v", err)
		}
	*/

	log.Println("Account info updated!")
}
