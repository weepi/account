package main

import (
	"encoding/base64"
	"flag"
	"log"
	"time"

	pb "gitlab.com/weepi/account/proto"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1664", "Endpoint to listen to")
	tokenFlag    = flag.String("token", "", "User token")

	profilePicture = `
iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABGdBTUEAALGPC/xhBQAAAAFzUkdC
AK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dE
AAAAAAAA+UO7fwAAAAlwSFlzAAAASAAAAEgARslrPgAAAAl2cEFnAAAAHgAAAB4A+E/hPQAACcxJ
REFUSMfllmtQ01caxp//JSHkyq0EQcAowUgKqJWbII6gM0TabYHalM4KmVqn2862Y0cZLbU6tmCL
fqB422mnpZcpxVoUZqsOI5cKshqtQKMdjBIvNIYxDZALk5jkn3/OftiR6nYv7X7d59N5Z86c33ln
nnPeB/h/E/VbN3Z0dIgAyCmK4trb2wXHjx8HIeSXgygKLMtCJpPhvffe8yqVSjoUCgWeffbZ4O8C
v/XWW9EDAwMZbre7huO45eFwOGb+/PkLExISZoVCoUwqlcLj8QAARCIRoqOjQdM0gsEgHA7HpN/v
Z4xG49mCgoI/ff31167/Cs7MzBT7fL7W9PR0XUFBgVypVEIqlUKhUEAoFIJlWSQnJ0OhUMDtdgMA
5HI5JicnYbPZUFRUBLFYDJ7nMTg4iLq6uqsajSbn2LFjgYc5zMOFSqXKlsvl53fv3r2ysrIyori4
GFKpFJGRkZDL5RCJRBAKhQiFQoiIiMCtW7fgdrshkUjg8/lw//59OJ1O2O12KJVKpKWlQalUKjs6
OuJ+/vnnU/8SfOTIEeXExETf0aNHU3JzcyEWi3HhwgVIJBKwLIvZ2Vmkp6dDrVZDqVSCpmlYLBak
paWBpmk89thjGB8fn7vY/fv3ERkZicWLF8NoNGatXbu29eLFi7O/AqtUqsLS0tI/FxcXAwCMRiNK
S0uRkJCApKQkOJ1OqFQq0DQNhmFw69YtZGdnIyYmZs5YWq0WAOB0OiGVSgEAUVFRkEqlzEcffeRx
OBwDD3j0g0V3d/ei1NRUUBQFnueRkZEx51ZCCCiKmqt5nkdiYiLGxsZQVVWFvLw8ZGVlQafT4ccf
f8SCBQuQlpaGlJQU0DSNwsJCRERE7CgrKxM8Ai4qKvrDvHnzDixfvhyEEIRCIbS0tGDNmjUoKCjA
ggULMDY2hmAwCJ7nAQCnTp1Cc3Mz9u7dC51OB7vdjvPnz+OTTz5BT08PzGYzfD4fCCG4fv06ampq
Im/fvv3T66+/vhAAmA0bNix0OBw99fX1ori4uLnuhEIhxGIxGIYBz/Noa2tDb28vVq5ciZiYGDQ2
NiI2NhZfffUVVqxYgbKyMmzatAlOpxNLliwBy7LweDwYHh5GIBBAWVkZPB6PdGhoaF19fX0bJZfL
17z22mv9q1evRigUwujoKObNm4fq6mqEw2EIBAKEQiE0NDRg1apVKCkpAU3T8Hg8GBwcxOrVqyGT
yfDxxx/DaDTi7bffhsvlQmNjI7xeL2pqajAzM4Pu7m4cOHAAra2taG9v38PSNP3AABgcHIROp8Px
48fx9NNPQyaTob29HRRFYWZmBtHR0WAYBhRFQaFQoLy8HDT9D5sYDAZoNBp8+umnEAgE2Lp1K+Lj
47Fz5058/vnn8Hq9uHPnDurr63H79u0tjEgkUhUWFtbGxMSguroaZ86cgUqlgsFggE6nQzgcxksv
vYQVK1Zg/fr1YFkWPM/j5MmTMBgM8Pl8UKvVEAgESElJwZo1a5Cfn49vvvkGhw8fBsdxmJqawrp1
63Dp0iXk5eVhdHRURPM8bzt27Bji4+Nht9vR1dWFe/fuITU1FYFAAA0NDRgZGcFTTz0FgUAAj8eD
xsZG9Pf3Y/v27QiHw7DZbLh69SosFguampqwbt06xMXFob29HdHR0UhNTcXg4CBSUlIwMTEBs9l8
hXnjjTdmTCZT+aZNmxLdbjc6OjrQ39+PVatWQaFQQKfT4YknnkBzczN8Ph9UKhXmz5+P7OxsDA8P
Q6VS4dKlSzhy5AiuXLmC3NxcxMXFYePGjdi5cydsNhveffdd2O12iMVidHR0wGg0tlAAUFVVVZyT
kzPwwgsv4PHHH8f69etx7do15OXlYcOGDdBoNBgbG8O1a9dQXFyMO3fu4PLlyzh79izu3r2LHTt2
oLa2Fn6/H83NzTh79iyCwSA4jsMXX3yBxYsXw2w2w2w2o6WlZSI2NnYhBQBCoZCtqKjg9u/fD61W
i3379mHZsmU4ePAg8vPzcfPmTYyMjCAzMxOEELjdbtTV1SE+Ph4nT57E7OwsGIbBwYMHMTU1hdra
WuTm5mJkZARNTU2gKApOpxPFxcUejUZT1NnZefXBR8JWVlYSq9VKGIYhUqmU6PV6snTpUtLX10du
3rxJXn31VTI+Pk46OzvJtm3byPj4OPnss8/I2rVriUwmI0lJSWTfvn3k3Llz5J133iF6vZ44HA7C
cRwJBoNk8+bNpLa2NvefpyFbWVlJbDYbEQgEhGVZwrIsEQqFj9QCgYAIhcK5WiqVkldeeYVs3ryZ
TE5OkmAwSCYmJkhmZia5fPky4TiOcBxHJicnyZIlS6w1NTW/TMPp6enIrq4ueVVVFQkEAuTJJ58k
GRkZJDk5mURFRRGhUEhEIhGJiIggSqWSaLVaUlJSQnp6eojVaiV+v5+8+OKL5O7du2RoaIjk5OSQ
77//nnAcR3ieJ36/n7z55pukoKCg8uFO6djY2PunT58O0DQNmqah0WgQFxeHbdu2Yc+ePVi2bBmG
hoag1+vR3d0Nk8mEsrIyZGVlISEhAQzDIDMzE3q9HocOHUJXVxcUCgX8fj8mJyfR2tqKM2fOHDUY
DF2PJJBz585RFRUVTGlpKffll19ienoadXV1uHHjBhiGgUqlQnl5OaxWKxITE1FRUYGmpiaoVCpM
T0/j9OnTSElJwfPPP4/09HSYzWbI5XIAwIkTJ9Db29vpcrmqrFYreQQMADRNs7t37+bq6urAMAzs
djvkcjksFguuXLkCi8UCj8czN0AUCgUWLVoErVaLpKQkcByHGzduQCwWY2ZmBmKxGHv37g1OTU0d
CgaD269fvx76lakAgGVZqNVqWCwWqNVqOBwOuFwuaDQaJCYmYmJiAhRFIRgMzoHVajWGh4dhMpkg
kUjg9XoRHR2Nvr4+DAwMXFy+fPnW77777m//LkyyAMAwzIMUgv7+fmRnZyMyMhI//PADGIZBOByG
1+sFTdOQy+UwmUywWq2QyWRIT0+H3+/Hhx9+iPPnz/dptdo9Op3uwvvvvx/CfxAFAAqFgn3uuee4
w4cPg6IojI2NwefzzaUOnuchEokgkUiQnJyM2dlZREVFwev1oqGhAd3d3VeXLl26S6/Xn3rmmWc4
/FZt2bKFUqvVfz1x4gQJBAIkEAiQcDhMeJ4nPM/PvUe/309sNhv54IMPSHV1NcnJyXGWlJTUZmVl
iX4z7OGOAWDXrl0xbW1tLfn5+X+sqamBWq0GIQThcBgmkwnffvstRkdHbYSQnwD8xWAwWAOBgLm+
vv7e74XOgTmOkwkEgtnKykrK5XLlSCSSjTRNL3zgYpfLNcgwTO/IyMj1l19+mTQ1NXn/F9jD+juC
TlVLfXcrawAAACV0RVh0Y3JlYXRlLWRhdGUAMjAxMC0wMi0xM1QwNDo1Mzo0MiswMDowMIqUX04A
AAAldEVYdG1vZGlmeS1kYXRlADIwMTAtMDItMTNUMDQ6NTM6NDIrMDA6MDDVJSl6AAAAGHRFWHRT
b2Z0d2FyZQBQYWludC5ORVQgdjMuMzap5+IlAAAAAElFTkSuQmCC`
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	client := pb.NewWeepiAccountClient(conn)

	// If the query took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	md := metadata.Pairs("authorization", *tokenFlag)
	ctx = metadata.NewOutgoingContext(ctx, md)

	img, err := base64.StdEncoding.DecodeString(profilePicture)
	if err != nil {
		panic(err)
	}

	if _, err = client.UploadProfilePicture(ctx, &pb.Picture{
		Picture: img,
	}); err != nil {
		log.Fatalf("Error updating account : %v", err)
	}

	log.Println("Profile picture updated!")
}
