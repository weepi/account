package main

import (
	"errors"
	"fmt"
	"time"

	pb "gitlab.com/weepi/account/proto"
	"gitlab.com/weepi/account/schemes"
	"gitlab.com/weepi/common/db/postgres"
	"gitlab.com/weepi/common/utils"

	"github.com/badoux/checkmail"
	"github.com/bearbin/go-age"
	"github.com/satori/go.uuid"
	pbauth "gitlab.com/weepi/auth/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc/metadata"
)

const (
	MinAgePolicy            = 18
	FirstNameMinLen         = 2
	FirstNameMaxLen         = 200
	MobilePhoneNumberLength = 5
	DefaultLatitude         = 48.8566
	DefaultLongitude        = 2.3522

	UniqueConstraintEmail             = "users_email_key"
	UniqueConstraintMobilePhoneNumber = "users_mobile_phone_number_key"
)

var (
	ErrInternal            = errors.New("Internal error")
	ErrFirstNameMinLen     = fmt.Errorf("A first name must be at least %d characters", FirstNameMinLen)
	ErrEmptyFirstName      = errors.New("A first name must be provided")
	ErrInvalidName         = errors.New("A valid name is mandatory")
	ErrInvalidEmailFormat  = errors.New("A valid email address is mandatory")
	ErrInvalidEmailHost    = errors.New("A valid email host is mandatory")
	ErrInvalidEmail        = errors.New("A valid email is mandatory")
	ErrExistingEmail       = errors.New("This email has already been taken")
	ErrEmptyEmail          = errors.New("An email address must be provided")
	ErrInvalidPictureUrl   = errors.New("A valid URL picture is mandatory")
	ErrInvalidPhoneNumber  = errors.New("A valid mobile phone number is mandatory")
	ErrExistingPhoneNumber = errors.New("This mobile number has already been taken")
	ErrInvalidLatitude     = errors.New("The latitude is not valid")
	ErrInvalidLongitude    = errors.New("The longitude is not valid")
	ErrInvalidDateOfBirth  = fmt.Errorf("The user must be more than %d years old", MinAgePolicy)
	ErrWeepiAuthPassword   = errors.New("Could not save the password")
	ErrNonExistantSport    = errors.New("The sport does not exist")
)

type InternalValidateError struct {
	err         error
	InternalErr error
}

func (e *InternalValidateError) Error() string {
	return e.err.Error()
}

func (s *WeepiAccount) validAccountInfos(req *pb.Account) error {
	// Check first name
	if req.GetFirstName().GetFirstName() == "" {
		return ErrEmptyFirstName
	}
	if len(req.GetFirstName().GetFirstName()) < FirstNameMinLen {
		return ErrFirstNameMinLen
	}

	// TODO: Check mobile phone number
	if req.GetMobilePhoneNumber().GetMobilePhoneNumber() == "" {
		return ErrInvalidPhoneNumber
	}

	// Check user age
	userDOB := time.Unix(req.GetDateOfBirth().GetDateOfBirth().GetSeconds(), 0)
	if userDOB.After(time.Now()) || age.Age(userDOB) < MinAgePolicy {
		return ErrInvalidDateOfBirth
	}

	// Email checking
	if req.GetEmailAddress().GetEmailAddress() == "" {
		return ErrEmptyEmail
	}

	if err := checkmail.ValidateFormat(req.GetEmailAddress().GetEmailAddress()); err != nil {
		return &InternalValidateError{
			err:         ErrInvalidEmailFormat,
			InternalErr: err,
		}
	}
	// Takes too much time
	/*
		if err := checkmail.ValidateHost(req.GetEmail()); err != nil {
			return &InternalValidateError{
				err:         ErrInvalidEmailHost,
				InternalErr: err,
			}
		}
	*/

	// GPS Coordinate checking
	latitude := req.GetLocation().GetLatitude()
	longitude := req.GetLocation().GetLongitude()
	if latitude < -90 || latitude > 90 {
		return ErrInvalidLatitude
	}
	if longitude < -180 || longitude > 180 {
		return ErrInvalidLongitude
	}

	// Sports UUID checking
	for _, UUID := range req.GetSportsUUIDs() {
		var sportExists bool
		if err := s.msg.Request("sport.exists", UUID.GetUuid(), &sportExists, 3*time.Second); err != nil {
			Logger.Error("Error getting existence of sport")
			return err
		}
		if !sportExists {
			return ErrNonExistantSport
		}
	}

	return nil
}

// Send user credentials to weepi auth
func (s *WeepiAccount) handleCredentials(ctx context.Context, c *Creds) error {
	creds := pbauth.Creds{
		Uuid: &pbauth.Uuid{
			Value: c.Uuid.String(),
		},
		Secret: &pbauth.Secret{
			Value: c.Secret,
		},
		AuthType: c.Type,
	}
	var errn utils.Error
	if err := s.msg.Request("auth.insert", &creds, &errn, 3*time.Second); err != nil {
		return err
	}
	if errn.Msg == "" {
		return nil
	}

	return errors.New(errn.Msg)
}

type Creds struct {
	Uuid   uuid.UUID
	Secret string
	Type   pbauth.AuthType
}

// TODO: Use context
// TODO: If picture URL is empty, provides a default one
func (s *WeepiAccount) Create(ctx context.Context, req *pb.Account) (*pb.Uuid, error) {
	Logger.Infof("Creating account for [%+v]...", req)

	// Check data
	if err := s.validAccountInfos(req); err != nil {
		if serr, ok := err.(*InternalValidateError); ok {
			Logger.With("error", serr.InternalErr).Error("internal validation process error")
		}
		Logger.With("error", err).Error("validation process failed")
		return nil, err
	}

	//If no latitude and longitude was sent set it to default value
	latitude := req.GetLocation().GetLatitude()
	longitude := req.GetLocation().GetLongitude()
	if latitude == 0 && longitude == 0 {
		latitude = DefaultLatitude
		longitude = DefaultLongitude
	}
	// Fill User struct
	newUser := &schemes.User{
		FirstName:         req.GetFirstName().GetFirstName(),
		MobilePhoneNumber: req.GetMobilePhoneNumber().GetMobilePhoneNumber(),
		DateOfBirth:       time.Unix(req.GetDateOfBirth().GetDateOfBirth().GetSeconds(), 0),
		Email:             req.GetEmailAddress().GetEmailAddress(),
		Latitude:          latitude,
		Longitude:         longitude,
		PictureURL:        req.GetPictureUrl().GetPictureUrl(),
	}
	for _, sp := range req.GetSportsUUIDs() {
		newUser.Sports = append(newUser.Sports, sp.GetUuid())
	}

	// Try Insert in db
	err := s.db.Create(newUser).Error
	if err != nil {
		Logger.With("error", err).Errorf("Inserting user [%s - %s] error",
			newUser.FirstName, newUser.MobilePhoneNumber)

		// Check if user already known
		if postgres.IsUniqueConstraintError(err, UniqueConstraintEmail) {
			return nil, ErrExistingEmail
		} else if postgres.IsUniqueConstraintError(err, UniqueConstraintMobilePhoneNumber) {
			return nil, ErrExistingPhoneNumber
		}

		return nil, errors.New("Could not save user details")
	}

	creds := Creds{
		Uuid:   newUser.UUID,
		Secret: req.GetSecret().GetSecret(),
		Type:   pbauth.AuthType(req.GetAuthType()),
	}

	md := metadata.Pairs("useruuid", uuid.Nil.String())
	ctx = metadata.NewOutgoingContext(ctx, md)
	if err = s.handleCredentials(ctx, &creds); err != nil {
		Logger.With("error", err).Error("error submiting password to weepi auth")

		// Delete newly inserted user in DB in this case
		if err := s.db.Delete(newUser).Error; err != nil {
			Logger.With("error", err).Error("could not delete user %s", newUser.UUID)
			return nil, ErrInternal
		}
		return nil, ErrWeepiAuthPassword
	}
	Logger.Debugf("account created with UUID: '%s'", newUser.UUID.String())

	// Gen resp
	return &pb.Uuid{
		Uuid: newUser.UUID.String(),
	}, nil
}
