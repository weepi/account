package main

import (
	"fmt"
	"gitlab.com/weepi/account/schemes"
	"gitlab.com/weepi/common/db"

	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/nats-io/go-nats"
)

type WeepiAccount struct {
	db         *db.DB
	s3Uploader *s3manager.Uploader
	msg        *nats.EncodedConn
}

func NewWeepiAccount() (*WeepiAccount, error) {
	// Db conn
	Logger.Infof("Attempting to connect to the database at %s...", Cfg.DBHost)
	d, err := db.New(&db.Config{
		Driver:   Cfg.DBDriver,
		Host:     Cfg.DBHost,
		User:     Cfg.DBUser,
		Name:     Cfg.DBName,
		SSLMode:  Cfg.DBSSLMode,
		Password: Cfg.DBPassword,
	}, Logger)
	if err != nil {
		return nil, err
	}
	d.LogMode(true)

	// Migrate the schemes
	if err = d.AutoMigrate(&schemes.User{}).Error; err != nil {
		return nil, err
	}

	Logger.Info("DB conn OK")

	// NATS conn
	dsn := fmt.Sprintf("nats://%s", Cfg.NATSAddr)
	nc, err := nats.Connect(dsn)
	if err != nil {
		Logger.With("error", err).Fatal("can't connect to NATS")
	}
	econn, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		Logger.With("error", err).Fatal("can't create encoded conn")
	}

	// Connect to Amazon S3
	up, err := newS3Uploader()
	if err != nil {
		Logger.With("error", err).Fatal("can't get s3 uploader")
	}

	return &WeepiAccount{
		db:         d,
		s3Uploader: up,
		msg:        econn,
	}, nil
}

func (s *WeepiAccount) Close() error {
	Logger.Info("Server is shutting down...")
	return s.db.Close()
}
