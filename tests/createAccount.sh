#!/bin/bash

echo "$(cat)" | polyglot \
	--command=call \
	--use_reflection=false \
	--proto_discovery_root=$GOPATH/src/gitlab.com/weepi/account/proto \
	--add_protoc_includes=/usr/include/,$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--endpoint=127.0.0.1:1664 \
	--full_method='.WeepiAccount/Create'

