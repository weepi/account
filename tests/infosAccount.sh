#!/bin/bash

echo "$(cat)" | polyglot \
	--command call \
	--use_reflection false \
	--proto_discovery_root $GOPATH/src/gitlab.com/weepi/account/proto \
	--add_protoc_includes /usr/include/,$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--metadata a:b \
	--endpoint localhost:1664 \
	--full_method='.WeepiAccount/Infos'
	--metadata authorization:EabkepOeWsChRWDaEiIYujDGVhtro9CFMxz7qO5M/yU=

